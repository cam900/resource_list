# Resource list library

ID based resource list class library

## License

This repository and files are licensed under MIT license, see details on [here](https://gitlab.com/cam900/resource_list/-/blob/main/LICENSE).

## Usage:

See top of resource_list.hpp
